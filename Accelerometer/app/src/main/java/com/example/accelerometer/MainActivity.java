package com.example.accelerometer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {


    private SensorManager sensorManager;
    Sensor accelerometer;

    ImageView iv_vspacial;
    ImageView jupiter;
    ImageView canopus;
    ImageView venus;
    ImageView earth;

    TextView txt_canopus;
    TextView txt_venus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        iv_vspacial = findViewById(R.id.vaisseau_spacial);
        earth = findViewById(R.id.shape_earth);
        canopus = findViewById(R.id.shape_canopus);
        jupiter = findViewById(R.id.shape_jupiter);
        venus = findViewById(R.id.shape_venus);
        txt_canopus= findViewById(R.id.txt_canopus);
        txt_venus= findViewById(R.id.txt_venus);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        // values[] -> index 0 : z | index 1 : x | index 2 : y

        RotateAnimation rotateAnimation = new RotateAnimation(-sensorEvent.values[0], sensorEvent.values[0], Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(500);
        rotateAnimation.setFillAfter(true);
        iv_vspacial.setAnimation(rotateAnimation);

        if(sensorEvent.values[0]>=6.0){
            txt_canopus.setBackgroundColor(Color.RED);
        } else {
            txt_canopus.setBackgroundColor(Color.BLACK);
        }

        if(sensorEvent.values[0]<=-5.46){
            txt_venus.setBackgroundColor(Color.YELLOW);
        } else {
            txt_venus.setBackgroundColor(Color.BLACK);
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

}